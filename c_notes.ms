.TL
C Notes
.AU Cody Wink
.AB no
This document contains notes on my preferred idioms, structure, and style of C
coding. Most of it has been picked up over the years while reading books on
UNIX/Linux and browsing code repositories such as OpenBSD's.
.AE
.SH
Alignment and Indentation
.PP
The argument for using either tabs or spaces for alignment, spacing,
indentation, etc. has been one of the most sensitive topics in programming
history. The rules I follow make an attempt to not only be atheistically
pleasing, but also practical. One rational for this is from an accessibility
perspective. People with poor eyesight might have issues with small spacing and
might need to expand it further.
.[
 tabsvsspaces
.]
Historically tabs were created for tabulating data which is synonymous with
alignment.
.LP
The general rule is to use tabs to tabulate data (for adjustable alignment), and
spaces to alignment to tabs. This basically means that a user should be able to
adjust their tab settings to whatever length they please and the alignment still
looks correct. The easy part is indentation, that is
.B always
a tab. Alignments are where things get tricky as you have to think about how
text is
.I tabulated .
.IP \[bu]
Enum's should be indented with tabs, and if an assignment is made, spacing added
to make the identifiers equal size, another tab, and then the assignment. An
example with a tab size of 8 is:
.DS
.ft CW
enum {
------->MAXBUFSZ------->= 512,
------->WINH    ------->= 640,
------->WINW    ------->= 480
};
.DE
With a tab size of 16:
.DS
.ft CW
enum {
--------------->MAXBUFSZ------->= 512,
--------------->WINH    ------->= 640,
--------------->WINW    ------->= 480
};
.DE
.IP \[bu]
Functions should be indented with tabs and have their parameters (if continued
on the next line) aligned with spaces. An example with a tab size of 8 is:
.DS
.ft CW
int
main(int argc,
     char *argv[])
{
------->printf("Hello, World!\n");
------->return EXIT_SUCCESS;
}
.DE
With a tab size of 16:
.DS
.ft CW
int
main(int argc,
     char *argv[])
{
--------------->printf("Hello, World!\n");
--------------->return EXIT_SUCCESS;
}
.DE
.IP \[bu]
Global variables
.B "not local" ) (
should have a tab after their type, followed by spaces to ensure all type
identifiers are the same width, followed by another tab, then either a space or
asterisk, then finally the name. If there is assignments (which for globals
there
.B should
be), then the name's should have spacing to ensure they're the same width,
followed by a tab, then the assignment. This is similar to contents in a
.CW struct
or
.CW union
combined with the assignment alignment of an
.CW enum .
An example with a tab size of 8 is:
.DS
.ft CW
static const char------>*curerr>= "Some error.";
static int       ------> delta >= 12;
.DE
With a tab size of 16:
.DS
.ft CW
static const char-------------->*curerr-------->= "Some error.";
static int       --------------> delta -------->= 12;
.DE
.IP \[bu]
Keywords and assignments have special alignment when broken apart. For keywords,
the code that is broken apart should align with the parenthesis much like
functions. For assignments, they should align with the text after the equal
sign. The text will almost always be broken at an operator, in which case the
operator will be at the end of the previous line.  An example with a tab size of
8 is:
.DS
.ft CW
int
foobar(void)
{
------->int i, j, k, l;
------->
------->i = j = k = l = 0;
------->if (i == j && j == k &&
------->    k == l)
------->-------->return 0;
------->i = 1 + 2 + 3 + 4 +
------->    5 + 6;
------->return i;
}
.DE
With a tab size of 16:
.DS
.ft CW
int
foobar(void)
{
--------------->int i, j, k, l;
--------------->
--------------->i = j = k = l = 0;
--------------->if (i == j && j == k &&
--------------->    k == l)
--------------->---------------->return 0;
--------------->i = 1 + 2 + 3 + 4 +
--------------->    5 + 6;
--------------->return i;
}
.DE
.IP \[bu]
Labels and
.CW goto
in general should be scarce so when we use labels they are
.B always
at column 0. No tabs, no indentation, just the label, i.e. for the label
.CW end :
.DS
.ft CW
int
foobar(void) {
------->/* some code */
end:
------->return rc;
}
.DE
.IP \[bu]
Preprocessor conditionals have special indentation that increases their
readability. They differ from defines and macros in that they don't have a tab
after the statement. They use a space, making them more similar to
non-preprocessor conditionals. Instead, all
.CW #
symbols stay on column 0 and the statements that follow are indented with tabs.
Alignment for arguments that continue onto the next line are the same as
non-preprocessor conditionals as well. You move to the current lines last tab
stop on the next line and add spaces until the arguments align. An example with a tab size of 8 is:
.DS
.ft CW
#ifdef DETECTOS
#------>if defined(__GNUC__) || \\
------->   defined(__unix__)
#------>------->define->OS----->1
#------>elif defined(_MSC_VER)
#------>------->define->OS----->2
#------>endif
#endif
.DE
An example with a tab size of 16 is:
.DS
.ft CW
#ifdef DETECTOS
#-------------->if defined(__GNUC__) || \\
--------------->   defined(__unix__)
#-------------->--------------->define--------->OS------------->1
#-------------->elif defined(_MSC_VER)
#-------------->--------------->define--------->OS------------->2
#-------------->endif
#endif
.DE
The
.B only
exception to this rule is for
.I "header guards" .
The 
.CW #ifndef
statement contains a tab after it, just like the
.CW #define
before it, i.e. for a tab size of 8:
.DS
.ft CW
#ifndef>HEADER_H
#define>HEADER_H
#endif
.DE
and then a tab size of 16:
.DS
.ft CW
#ifndef-------->HEADER_H
#define-------->HEADER_H
#endif
.DE
.IP \[bu]
Preprocessor constants are indented with a tab, then the name followed by spaces to make all identifiers the same with, then another tab. An example with a tab size of 8 is:
.DS
.ft CW
#define>ERROOM  ------->"Out of Memory"
#define>WINTITLE------->"Example Window"
.DE
With a tab size of 16:
.DS
.ft CW
#define-------->ERROOM  ------->"Out of Memory"
#define-------->WINTITLE------->"Example Window"
.DE
.IP \[bu]
Preprocessor macros are indented with a tab, then the name followed by spaces
to make all identifiers (with their parameters) the same with, then another tab.
An example with a tab size of 8 is:
.DS
.ft CW
#define>CALLERR(MSG)--->err(MSG)
#define>MIN(A, B)   --->((A) > (B) ? (A) : (B))
.DE
With a tab size of 16:
.DS
.ft CW
#define-------->CALLERR(MSG)--->err(MSG)
#define-------->MIN(A, B)   --->((A) > (B) ? (A) : (B))
.DE
Many times a macro will expand to a block of code. For simple wrappers to
functions or ternary statements we can just leave on one line (unless it exceeds
the line limit), but if you have a more complex
.CW "if, else if, or else"
chain, then you'll place that code on the next line, adding a space and then a
backwards slash to the end of all lines (except the last), aligning them all to
to one character past the longest line.
All lines in this code block should be indented by 1 tab. An example with a tab
size of 8 is:
.DS
.ft CW
#define>EPRINTIF(COND, MSG)                         \\
------->if ((COND))                                 \\
------->------->fprintf(stderr, "ERROR: %s\n", MSG)
.DE
With a tab size of 16:
.DS
.ft CW
#define-------->EPRINTIF(COND, MSG)                         \\
--------------->if ((COND))                                 \\
------------------------------->fprintf(stderr, "ERROR: %s\n", MSG)
.DE
Notice that the final backward slashes don't align correct. This is not really
something that can be handled naturally, so we align them on a tab setting of 8.
.IP \[bu]
Prototypes should have the same spacing for their return types, followed by a
tab, then either an asterisk or space, and then the function name. If a
functions parameters continue on the next line, they should use spaces
continuing from the tab that preceded the functions name. An example with a tab
size of 8 is:
.DS
.ft CW
int         -------> area(int,
            ------->      int);
char        ------->*err(void);
struct point------->*getcoord(void);
.DE
With a tab size of 16:
.DS
.ft CW
int         ---------------> area(int,
            --------------->      int);
char        --------------->*err(void);
struct point--------------->*getcoord(void);
.DE
.IP \[bu]
Structures and Unions should be indented with tabs, have the same spacing for
their types, followed by a tab, then either an asterisk or space, and then the
name. This is similar to how prototypes are. An example with a tab size of 8 is:
.DS
.ft CW
struct str {
------->char         ------->*data;
------->unsigned long-------> cap;
------->unsigned long-------> len;
};

union num {
------->double-------> real;
------->long  -------> integ;
};
.DE
With a tab size of 16:
.DS
.ft CW
struct str {
--------------->char         --------------->*data;
--------------->unsigned long---------------> cap;
--------------->unsigned long---------------> len;
};

union num {
--------------->double---------------> real;
--------------->long  ---------------> integ;
};
.DE
.IP \[bu]
Switch statements should
.B never
have its
.I case
keywords indented. Only the contents of the case keywords should be indented. An
example with a tab size of 8 is:
.DS
.ft CW
switch (num) {
case 1:
------->i = num * 2;
------->break;
case 2:
------->i = num * 3;
------->break;
default:
------->break;
}
.DE
With a tab size of 16:
.DS
.ft CW
switch (num) {
case 1:
--------------->i = num * 2;
--------------->break;
case 2:
--------------->i = num * 3;
--------------->break;
default:
--------------->break;
}
.DE
.LP
It's important to note that the max amount of characters per line should be no
longer than
.B 79
with a tab width of
.B 8 .
.SH
Brace Style
.PP
Another point of contention style and formatting is where to place braces. The
style used here is taken straight from the OpenBSD manpage.
.[
openbsdman
.]
It follows the
.B KNR
form which is based upon
.B K&R .
In general braces should not be used. If the following code is only a single
line, just indent it with a tab and that's it. It should also be mentioned to
.B "never place the following statement on the same line as a keyword" .
.LP
The general rule is that an opening brace is always after a keyword or the
continuation of a keyword block (i.e. for an
.CW if
statement), and the closing brace is always on its own line, with a continuation
keyword following it (if there is one). The only exception to this rule is
function
.I definitions .
For these, both the opening and closing braces are on their own lines.
.IP \[bu]
A
.CW for
loop with no braces should be in the form of:
.DS
.ft CW
for (i = 0; i < 5; ++i)
	buf[i] = 0;
.DE
A
.CW for
loop with braces should be in the form of:
.DS
.ft CW
for (i = 0; i < 5; ++i) {
	buf[i] = 0;
	++num;
}
.DE
.IP \[bu]
A
.CW while
loop with no braces should be in the form of:
.DS
.ft CW
while ((err = foobar(&msg)) >= 0)
	printf("%s\n", msg);
.DE
A
.CW while
loop with braces should be in the form of:
.DS
.ft CW
while ((err = foobar(&msg)) >= 0) {
	printf("%s\n", msg)
	++num;
}
.DE
.IO \[bu]
A
.CW switch
statement must always have braces, but it is
.I optional
for cases. If you must have braces for a case, then the opening should be on the
same line as the
.CW case
keyword and the closing brace on its own line with the option
.CW break
directly after it.
.DS
.ft CW
switch (num) {
case 1:
	i = num * 2;
	break;
case 2: {
	int j;

	j = 3;
	i = num * j;
} break;
default:
	break;
}
.DE
.IP \[bu]
The chain of
.CW if ,
.CW "else if" ,
and
.CW "else"
become very complex very quick, but when chaining them together the general rule
applies to
.I each
link in the chain. The
.CW if
statement with no braces should be in the form of:
.DS
.ft CW
if (a == 1)
	return -1;
.DE
If we also include
.CW "else if"
and
.CW "else" ,
we get:
.DS
.ft CW
if (a == 1)
	return -1;
else if (a == 2)
	return -2;
else
	return -3;
.DE
An
.CW if
statement with braces should be in the form of:
.DS
.ft CW
if (a == 1) {
	++b;
	++c;
}
.DE
If we also include
.CW "else if"
and
.CW "else" ,
we get:
.DS
.ft CW
if (a == 1) {
	++b;
	++c;
} else if (a == 2) {
	b += 2;
	c += 2;
} else {
	b += 3;
	c += 3;
}
.DE
Now if we have any
.I links
in this chain that can be a single statement, then we should omit the braces. A
few examples of this are:
.DS
.ft CW
if (a == 1)
	++b;
else if (a == 2) {
	b += 2;
	c += 2;
} else {
	b += 3;
	c += 3;
}

if (a == 1) {
	++b;
	++c;
} else if (a == 2)
	b += 2;
else {
	b += 3;
	c += 3;
}

if (a == 1) {
	++b;
	++c;
} else if (a == 2) {
	b += 2;
	c += 2;
} else
	b += 3;
.DE
.IP \[bu]
Now for function
.B definitions ,
we have the open brace and closing brace on their own lines like so:
.DS
.ft CW
void
foobar(void)
{
	/* code */
}
.DE
.SH
Comment Style
.PP
Comments should be scarce. Not due to the lack of need to explain code, but
code should be written in such a way that it can be read with ease; however
this won't always be the case. Some times a user doesn't have immediate access
to a libraries source code and can only rely on the header and will need to
know details such as what the returned value represents, or what state has
possibly changed.
.LP
There are four comment styles, all of which should follow proper English
grammar, spelling, structure, and punctuation.
.IP \[bu]
File header comments should be in the multi line form describing the file
(usually headers) contents.
.DS
.ft CW
/*
 * An API for parsing CSV data.
 */
.DE
.IP \[bu]
Single line comments are simple and mostly explain something that isn't obvious,
i.e. a variable or function that you're unable to make short enough and need to
use uncommon abbreviations for.
.DS
.ft CW
/* Context passed to the callback provided by the user. */
.DE
.IP \[bu]
Multi line Comments that aren't simple and need to expand multiple lines.
Usually used to explain a non-obvious function or implementation for said
function.
.DS
.ft CW
/*
 * This implementation uses the FNV-1a hashing algorithm as
 * opposed to another such as the Jenkins hasing algorithm.
 * The rational for this is that FNV1-a provides less
 * collisions for this type of data at minimal loss of
 * speed.
 */
.DE
.LP
Comments may also be tagged to indicate there are special circumstances in the
code that may need additional attention. Tags aren't a part of the C language
but many editors support syntax highlighting of some of them.
.IP \[bu]
.CW FIXME
indicates that there is either a bug that will need resolution later, or there
isn't a better way of implementing code at the moment and needs cleanup.
.DS
.ft CW
/*
 * FIXME: Right now we perform a linear lookup but should
 *        probably switch to a hash table later.
 */
.DE
.IP \[bu]
.CW TODO
indicates that there is more work to be done, i.e. if an API is incomplete at
the moment and will require more functionality later.
.DS
.ft CW
/*
 * TODO: Currently this API lacks functionality to erase
 *       characters in its persisted data. This is a
 *       feature that we should implement eventually.
 */
.DE
.IP \[bu]
.CW XXX
is used if FIXME and TODO don't apply. This tag indicates that the comment needs
attention and it doesn't fall under the other categories.
.DS
.ft CW
/*
 * XXX: During writing this code it gave me an idea for
 *      possibly extending this API to include some
 *      non-standard conventions. I should discuss this
 *      later with my team.
 */
.DE
.SH
Functions and Prototypes
.PP
All functions, both private and public, should have a prototype. Both functions
and prototypes are formatted differently for both readability but also
searchability (i.e. using a tool such as
.CW grep ")."
Another reason is that functions, unlike prototypes, have potential to be very
lengthy.
.LP
A function prototype should never have identifiers for its parameters. The same
goes for the
.CW const
specifier. For instance, the following is the
.B correct
way to declare a function:
.DS
.ft CW
int	 area(int, int);
.DE
The following is the
.B incorrect
way to declare a function:
.DS
.ft CW
int	 area1(const int, const int);
int	 area2(int w, int h);
.DE
When defining a function, specifiers, attributes, and the type go on the line
.I before
the name of the function. The opening and closing braces also go on their own
line (this is the only case this happens). It should obviously have names for
its arguments as it's a requirement:
.DS
.ft CW
int
area(int w, int h)
{
	return w * h;
}
.DE
While empty lines are acceptable at a global level, function bodies should only
ever have one empty line. That is between the declarations and statements:
.DS
.ft CW
int
zero(unsigned char *buf, unsigned long len)
{
	int i;

	if (buf == NULL || len == 0)
		return -1;
	for (i = 0; i < len; ++i)
		buf[i] = 0;
	return 0;
}
.DE
.SH
Header Guarding
.PP
During the translation phase of compilation all included files are concatenated
together. This poses a problem when you include one header file in multiple
places as this can cause declarations to get redeclared. If the compiler runs
into multiple declarations it will fail to compile. To prevent this problem,
headers can be guarded using the preprocessor.
.LP
To start the process of guarding a header file, add the following to the top of
the file:
.DS
.ft CW
#ifndef	NAME_H
#define	NAME_H
.DE
and then add the following to the end of the file:
.DS
.ft CW
#endif
.DE
Note that the identifier
.CW UTIL_H
here is considered to be a
.I "macro constant" ,
so it follows the same naming conventions.
.SH
Local Variables
.PP
Variables that are local to a function are treated differently than global
variables. Their formatting and naming differs drastically from globals due
mostly to the fact the scope is so small that conserving space is much more
important than readability. In fact, for some local variables, readability is
not needed at all.
.LP
For instance, a global
.I index
variable might need a name such as
.CW stridx
to indicate to the user it's an index to a string. The name is also unique
enough to make browsing through the code to find its use trivial:
.DS
.ft CW
static unsigned long	stridx	= 0;

/* 1000 lines later */

strdata[stridx++] = ch;
.DE
But for a local index variable, the letter
.I i
should be sufficient since most likely it'll be used a few lines below its
declaration, so no explanation is needed:
.DS
.ft CW
char buf[BUFSZ];
int i;

for (i = 0; i < BUFSZ; ++i)
	buf[i] = '\0';
.DE
.LP
We also can omit much of the formatting with local variables as well, along with
the requirement to
.I initialize
each variable. Multiple variables of the same type can and should be placed on
the same line:
.DS
.ft CW
int i, j, k;
char buf[BUFZ], *ptr;
.DE
When assigning values to variables, if and only if they are the
.B "same type" ,
they may be assigned on the same line:
.DS
.ft CW
int i, j, k;

i = j = k = 0;
.DE
.SH
Macros with Semicolons
.PP
When writing a macro that simulates a function, it should always require a
semicolon after it. There are cases where this may seem impossible but there is
actually an idiom for this. You surround the macro contents with the code
.CW "do {} while(0)" .
This is not only useful for adding semicolons, but also allows you to declare
variables inside your macro; however, this is something I would never use unless
I absolutely had no option given the drastic side effects that can occur.
.LP
An example of this idiom is:
.DS
.ft CW
#define	MACRO(A, B, C, D) do { \\
	if ((A) == (B)) {      \\
		C = (A);       \\
		D = (B);       \\
	} else {               \\
		C = (B);       \\
		D = (A);       \\
	}                      \\
} while(0)

/* in a function somewhere... */
MACRO(a, b, c, d);
.DE
.SH
Naming Conventions
.PP
Having a standard naming convention for everything is important as it prevents
all sorts of issues. These issues range from name clashing, hand fatigue,
interfacing confusion, function finding, etc. The naming conventions outlined
here are taken mostly from K&R and KNF with a few personal changes.
.LP
The rule of thumb in general for all identifiers is that they are
.B "only lowercase and alpha numeric" .
This includes variable names, function names, global identifiers, private
identifiers, etc. The exceptions to this rule are:
.IP \[bu]
Any
.I constant
or preprocessor
.I macro
should be
.B "only uppercase and alpha numeric" .
.DS
.ft CW
#define	WINTITLE	"Example Window"

#define	ABS(A)	((A) > 0 ? (A) : (-1 * (A)))

enum {
	MAXBUFSZ	= 512
};
.DE
.IP \[bu]
Header guards should be
.B "only uppercase and alpha numeric"
with the exception that when a filename contains an
.I underscore
or
.I period
we use an
.B underscore
in the name. For example, if we have a file
.CW lib_utils.h ,
we would have:
.DS
.ft CW
#ifndef	LIB_UTILS_H
#define	LIB_UTILS_H
#endif
.DE
.IP \[bu]
Namespaces are used as a
.I prefix
to the identifier separated by an
.B underscore
whether it is a
.I library
level namespace, or a
.I type
namespace. For a library named
.I libfoo
that contains the types
.CW "enum err"
and
.CW "struct str" ,
we'd have:
.DS
.ft CW
enum foo_err {
	FOO_ERR_NONE	= 0,

	FOO_ERR_ALLOC,
	FOO_ERR_IDX,

	FOO_ERR_MAX
};

struct foo_str	*foo_str_new(void);
.DE
.LP
Where possible abbreviations should be used. While poorly chosen abbreviations
can be confusing, well chosen ones can be just as readable as their full name.
It also has the added benefit of reducing line splitting and improve typography.
Some examples are:
.TS
box center tab(#);
Cb Cb
LfCW LfCW.
Word#Abbreviation(s)
pointer#ptr
index#i
MAXSIZEOFBUFFER#MAXBUFSIZE, MAXBUFSZ
OUTOFMEMORYERROR#OUTOFMEMERR, OOMERR
allocatememory()#allocmem()
mylib_string_appendchar#myl_str_appch
.TE
This may seem somewhat trivial, but it does play a very important role when we
start having to namespace identifiers as names become very lengthy.
.SH
Organization of Code Content
.PP
It's important for many reasons to organize not only files but the code inside
them. It not only creates consistency with the source, but also ensures the
compilation process translates the code correctly. An example of this would be
including a system file that defines a preprocessor constant before a third
party
library to ensure the systems constant is used instead.
.LP
The following numbered list contains the base organization of any file
containing code (in order):
.IP 1.
Include Statements
.DS
.ft CW
#include	<sys/poll.h>
#include	<sys/select.h>

#include	<stdio.h>
#include	<stdlib.h>

#include	"err.h"
#include	"util.h"
.DE
.IP 2.
Preprocessor Constants
.DS
.ft CW
#define	EPSILON 	0.00001
#define	WINTITLE	"Example Window"
.DE
.IP 3.
Preprocessor Macros
.DS
.ft CW
#define	ABS(A)   	((A) > 0 ? (A) : (-1 * (A)))
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
.DE
.IP 4.
Enum Constants
.DS
.ft CW
enum {
	MAXBUFSZ 	= 512,
	WINHEIGHT	= 480,
	WINWIDTH 	= 640
};
.DE
.IP 5.
Typedefs
.DS
.ft CW
typedef	signed char  	schar;
typedef	unsigned char	uchar;
.DE
.IP 6.
Enum Types
.DS
.ft CW
enum err {
	ERR_NONE	= 0,

	ERR_ALLOC,
	ERR_IDX,

	ERR_MAX
};
.DE
.IP 7.
Structs
.DS
.ft CW
struct str {
	char         	*data;
	unsigned long	 cap;
	unsigned long	 len;
};
.DE
.LP
There are special rules that add more to this list depending upon whether the
file is an interface or implementation. For a interface (header) file we have:
.IP 8.
Public Function Declarations
.DS
.ft CW
struct point	*point_make(int, int);
int         	 point_x(const struct point *);
int         	 point_y(const struct point *);
.DE
.LP
For an implementation file we have:
.IP 8.
Private Function Declarations
.DS
.ft CW
static void	 parsespace(struct parser *);
static void	 parseword(struct parser *);
.DE
.IP 9.
Private Global Variables
.DS
.ft CW
static const char	*errstr	= "Some error.";
static int       	 delta 	= 12;
.DE
.IP 10.
Private Function Definitions
.DS
.ft CW
static void
parsespace(struct parser *pars)
{
	/* code */
}

static void
parseword(struct parser *pars)
{
	/* code */
}
.DE
.IP 11.
Public Function Definitions
.DS
.ft CW
struct point *
point_make(int x, int y)
{
	/* code */
}

int
point_x(const struct point *pt)
{
	/* code */
}

int
point_y(const struct point *pt)
{
	/* code */
}
.DE
.LP
When including headers they should be grouped by header type with each group
being sorted alphabetically. These groupings are (in order):
.IP 1.
System
.RS
Files that belong to the system, i.e. kernel files, drivers, etc.
.RE
.IP 2.
Global
.RS
Files that belong to libraries installed on the system, such as
libc, pthreads, etc. These may also be split up and order if needed.
.RE
.IP 3.
Local
.RS
Files that belong to the project itself, such as a utility header, or
files in your projects include folder.
.RE
.LP
Files that are local to the project should be included using quotations, i.e.
.DS
.ft CW
#include	"local.h"
.DE
Headers that are not local to the project such as system and global libraries
should be included using carrots, i.e.
.DS
.ft CW
#include	<stdio.h>
.DE
A complete example of a header inclusion would be:
.DS
.ft CW
#include	<sys/shm.h>
#include	<sys/types.h>

#include	<stdio.h>
#include	<stdlib.h>

#include	"queue.h"
#include	"util.h"
.DE
.LP
There also should be a function prototype for every function, including private
ones.

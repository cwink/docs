.POSIX:
.SUFFIXES:

REFER  	= refer
REFLAGS	= -e
ROFF   	= groff
RFLAGS 	= -ms
TBL    	= tbl

.SUFFIXES: .ms
.ms:
	$(REFER) $(REFLAGS) -p "$(*).msr" "$(<)" | \
		$(TBL) | \
		$(ROFF) $(RFLAGS) >"$(*).ps"
